use strict;
use warnings;
use lib "./lib";
use Logger;

my $logger = Logger->new(
    owner_type => "team",
    owner_name => "dev",
    app_type   => "app",
    app_name   => "logging-example"
);

$logger->log("INFO", "Hello World.");
$logger->log("CRITICAL", "HELP!");
$logger->log("OK", "Nothing to see here! Please disperse!");
